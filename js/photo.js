/*Не внося изменений в файл photo.html, разработать скрипт, который скроет все фотографии, размещённые в блоке 
с id="photo", кроме первых 4-х, и добавит на страницу 2 элемента интерфейса:
Следующие фото (По клику будет скрывать текущие фотографии и показывать 4 следующих фотографии, 
в случае, если оставшихся фотографий будет меньше 4-х, недостающие фотографии будут браться из начала фото-ряда)
Предыдущие фото (По клику будет скрывать текущие фотографии и показывать 4 предыдущих фотографии, 
в случае, если оставшихся фотографий будет меньше 4-х, недостающие фотографии будут браться из конца фото-ряда)*/

var n = 4; // number of photos to show each moment
var photos; // all <a> with photos
var fs = 0; // fs % photos.length is equal to index of the first shown element
var w; // width of each image
var l; // total number of photos in the gallery
window.onload = function() {
	var photoList = document.getElementById('photo');// <div id='photo'...
	photos = photoList.children;
	l = photos.length;
	w = photos[0].children[0].clientWidth; // assume each image has the same width
	for (var i = 0; i < l; i++) {
		photos[i].style.position = 'relative'; // [Work In Progress] for changing position of visible images
		if (i >= n)
			photos[i].style.visibility = 'hidden';
	}
	var nextPhoto = document.createElement('input');
	nextPhoto.setAttribute('type', 'button');
	nextPhoto.setAttribute('value', 'Next');
	nextPhoto.setAttribute('onclick', 'showNextPhotos()');
	document.body.appendChild(nextPhoto);
	var prevPhoto = document.createElement('input');
	prevPhoto.setAttribute('type', 'button');
	prevPhoto.setAttribute('value', 'Previous');
	prevPhoto.setAttribute('onclick', 'showPrevPhotos()');
	document.body.appendChild(prevPhoto);
}
function showNextPhotos() {
	for (var i = fs; i < fs + n ; i++) {
		photos[i % l].style.visibility = 'hidden';
		photos[(i + n) % l].style.visibility = 'visible';
	}
	fs += n;
}
function showPrevPhotos() {
	for (var i = fs - n; i < fs ; i++) {
		photos[(i + l) % l].style.visibility = 'visible';
		photos[(i + n) % l].style.visibility = 'hidden';
	}
	fs += photos.length - n;
}
